const path = require("path");
const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");

require('dotenv').config({ path: './.env' });

const produtionMode = process.env.NODE_ENV === "production";
const CopyPlugin = require("copy-webpack-plugin");


module.exports = {
    mode: (process.env.NODE_ENV ? process.env.NODE_ENV : 'development'),
    entry: "/src/index.js",
    output: {
        path: path.resolve(__dirname, "public"),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "@babel/preset-env",
                            "@babel/preset-react"
                        ],
                    },
                },
            },
            {
                test: /\.(s[ac]ss|css)$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'resolve-url-loader',
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true, // <-- !!IMPORTANT!!
                        }
                    }
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            esModule: false,
                        },
                    },
                ],
            },
        ],

    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
        }),
        new webpack.DefinePlugin({
            "process.env": JSON.stringify(process.env)
        }),
        new webpack.ProgressPlugin((percentage, message) => {
            console.log(`${(percentage * 100).toFixed()}% ${message}`);
        }),
        new CopyPlugin({
            patterns: [
                { from: "./assets", to: "assets" },
            ],
        }),
    ],
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    devServer: {
        static: {
            directory: path.resolve(__dirname, './assets'),
            publicPath: '/assets'
        },
        open: true,
        historyApiFallback: true, // use for react router fallback to index.html,
        client: {
            progress: false,
        },
        port: 9090, // define port
    },
    resolve: {
        alias: {
            '@components': path.resolve(__dirname, "./src/components/"),
            '@pages': path.resolve(__dirname, "./src/pages/"),
            '@providers': path.resolve(__dirname, "./src/providers/"),
            '@api': path.resolve(__dirname, "./src/api/"),
            '@utils': path.resolve(__dirname, "./src/utils/"),
            '@constants': path.resolve(__dirname, "./src/constants/")
        }
    },
};