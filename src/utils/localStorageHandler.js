
export function getItem(key) {
    try {
        return JSON.parse(localStorage.getItem(key));
    } catch (error) {
        console.error(error);
        return "";
    }
};

export function setItem(key, value) {
    localStorage.setItem(key, JSON.stringify(value))
};

export function removeItem(key) {
    localStorage.removeItem(key);
}

export function clearAll() {
    localStorage.clear();
}