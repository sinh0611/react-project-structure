import { toast } from "react-toastify";
import { isObject, isString, isNumber } from "lodash";
import { TOAST_TYPES } from "@constants/constants";
import jwtDecode from "jwt-decode";

export function formatErrorData(error) {
    console.error(isObject(error) ? error.stack || error : isString(error) ? error : String(error));

    return isObject(error) ? error.message || error.stack : isString(error) ? error : String(error);
}

const _showToastMsg = (msgTxt, type, autoClose = 2000) => {
    const configs = {
        position: "top-right",
        autoClose: autoClose,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        containerId: "binance-copy-trade",
    };
    if (type === TOAST_TYPES.SUCCESS) {
        toast.success(msgTxt, configs);
    } else if (type === TOAST_TYPES.ERROR) {
        toast.error(msgTxt, configs);
    }
};


export const showToastMessageSuccess = (msgTxt) => {
    _showToastMsg(formatErrorData(msgTxt), TOAST_TYPES.SUCCESS);
};
export const showToastMessageError = (msgTxt) => {
    _showToastMsg(formatErrorData(msgTxt), TOAST_TYPES.ERROR, 3500);
};

export function isTimeExpired(token) {
    let decodedToken = jwtDecode(token);
    //check token expired or not
    if (
        !decodedToken ||
        (decodedToken && !isNumber(decodedToken.exp)) ||
        (decodedToken && isNumber(decodedToken.exp) && decodedToken.exp < Date.now() / 1000)
    ) {
        return true;
    }
    return false;
}