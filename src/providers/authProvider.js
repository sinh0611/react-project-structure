import React, { useReducer, useMemo } from "react";
import * as localStorageHandler from "@utils/localStorageHandler";
const AuthContext = React.createContext();


const initialState = {
    userProfile: {},
    isAuth: false,
    error: ""
};

function authReducer(state, action) {
    switch (action.type) {
        case "AUTH_SUCCESS": {
            return {
                ...state,
                userProfile: action.userProfile,
                isAuth: true
            }
        }
        case "AUTH_FAILED": {
            return {
                ...state,
                userProfile: {},
                isAuth: false,
                error: action.error
            }
        }
        case "LOG_OUT": {
            return {
                ...state,
                userProfile: {},
                isAuth: false,
            }
        }
        default: {
            throw new Error(`Unsupported action type: ${action.type}`);
        }
    }
}

function AuthProvider({ children }) {
    const [state, dispatch] = useReducer(authReducer, initialState);
    const value = useMemo(() => [state, dispatch], [state]);

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

function useAuth() {
    const context = React.useContext(AuthContext);
    if (!context) {
        throw new Error(`useAuthState must be used within a AuthProvider`);
    }
    const [state, dispatch] = context;

    return {
        state,
        user: state.userProfile,
        isAuth: state.isAuth,
        setAuthData: (userProfile) => dispatch({ type: "AUTH_SUCCESS", userProfile }),
        logout: () => {
            localStorageHandler.clearAll();
            dispatch({ type: "LOG_OUT" })
        }
    };
}

export { AuthProvider, useAuth };