import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { AuthProvider } from "./providers/authProvider";
import { ThemeProvider, createTheme } from "@mui/material";
import CssBaseline from '@mui/material/CssBaseline';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const theme = createTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 900,
            lg: 1200,
            xl: 1536,
        },
    }
})

const AppRoute = () => {
    return (
        <ThemeProvider theme={theme}>
            <AuthProvider>
                <CssBaseline />
                <App />
                <ToastContainer />
            </AuthProvider>
        </ThemeProvider>

    )
}

ReactDOM.render(<AppRoute />, document.getElementById("app"));