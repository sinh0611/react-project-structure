import { commonRequest } from "./axios";


export function login({ email, password }) {
    const payload = {
        email,
        password
    };
    return commonRequest.post('/users/login', payload);
}

export function signup({ name, email, password }) {
    const payload = {
        name,
        email,
        password
    };
    return commonRequest.post('/users/register', payload);
}