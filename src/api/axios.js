import { formatErrorData } from "@utils/utilitiesService";
import axios from "axios";

const env = process.env;


const APP_DOMAIN = process.env.NODE_ENV !== "development" ? env.DOMAIN_PRODUCTION : env.DOMAIN_DEV;



const commonRequest = axios.create({
    baseURL: APP_DOMAIN,
    headers: {
        'Content-Type': 'application/json',
    },
});

// Add a response interceptor
commonRequest.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response?.data?.data;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error?.response?.data?.error);
});

const authRequest = axios.create({
    baseURL: APP_DOMAIN,
    headers: {
        'Content-Type': 'application/json',
        "Authorization": "token"
    }
});



export {
    commonRequest,
    authRequest
}