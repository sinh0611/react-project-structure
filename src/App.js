import React, { lazy, Suspense } from "react";
import {
    BrowserRouter,
    Routes,
    Route,
    Navigate,
} from "react-router-dom";
import LoadingScreen from "@components/LoadingScreen";

const LoginPage = lazy(() => import("./pages/LoginPage"));
const HomePage = lazy(() => import("./pages/HomePage"));
const SignupPage = lazy(() => import("./pages/SignupPage"));
const NoMatch = lazy(() => import("@components/NoMatch"));
const AuthLayout = lazy(() => import("./layouts/AuthLayout"));
const MainLayout = lazy(() => import("./layouts/MainLayout"));


import { useAuth } from "./providers/authProvider";


const PrivateRoute = ({ children }) => {
    const { isAuth } = useAuth();
    if (!isAuth) return <Navigate to="/login" replace />
    return children;
}


function App() {
    return (
        <BrowserRouter>
            <Suspense fallback={<LoadingScreen />}>
                <Routes>
                    <Route element={<MainLayout />}>
                        <Route
                            path="/"
                            element={
                                <PrivateRoute>
                                    <HomePage />
                                </PrivateRoute>
                            }
                        />
                    </Route>
                    <Route element={<AuthLayout />}>
                        <Route path="/login" element={<LoginPage />} />
                    </Route>
                    <Route element={<AuthLayout />}>
                        <Route path="/register" element={<SignupPage />} />
                    </Route>
                    <Route path="*" element={<NoMatch />} />
                </Routes>
            </Suspense>

        </BrowserRouter>
    )
}

export default App;