
import React, { useEffect } from "react";
import { Outlet, useNavigate, useLocation } from "react-router-dom";
import { makeStyles } from "@mui/styles";
import * as localStorageHandler from "@utils/localStorageHandler";
import { isTimeExpired } from "@utils/utilitiesService";
import { useAuth } from "@providers/authProvider";


const useStyles = makeStyles(theme => ({
    container: {
        width: "100vw",
        height: "100vh",
        backgroundImage: "url(/assets/imgs/loginBackground.jpeg)",
        backgroundSize: "cover",
        display: "flex",
        justifyContent: 'center',
        alignItems: "center"
    }
}));

const AuthLayout = () => {
    const classes = useStyles();
    const navigate = useNavigate();
    const { pathname } = useLocation();
    const { setAuthData, logout } = useAuth();
    useEffect(() => {
        const token = localStorageHandler.getItem("token");
        const userProfile = localStorageHandler.getItem("userProfile");
        if (token && userProfile && !isTimeExpired(token)) {
            setAuthData(userProfile);
            navigate("/");
        } else {
            logout();
        }
    }, [pathname])
    return (
        <div className={classes.container}>
            <div className={classes.content}>
                <Outlet />
            </div>
        </div>
    )
}

export default AuthLayout;
