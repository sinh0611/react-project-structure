import React, { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import TextInput from "@components/UI/TextInput";
import CustomCheckbox from '@components/UI/CustomCheckbox';
import { Field, Form, Formik } from 'formik';
import * as yup from "yup";
import { makeStyles } from "@mui/styles";
import * as AuthService from "@api/auth";
import { useNavigate, Link } from "react-router-dom";
import SpinnerLoading from '@components/UI/SpinnerLoading';
import { showToastMessageError, showToastMessageSuccess } from '@utils/utilitiesService';
import * as localStorageHandler from "@utils/localStorageHandler";
import { useAuth } from '@providers/authProvider';


const useStyles = makeStyles(theme => ({
    container: {
        background: "#ffffff",
        padding: "24px 16px",
        borderRadius: 8
    },
    form: {
        minWidth: 440
    }
}));


const initialValues = {
    email: "",
    password: "",
    isRemember: false
}

const validationSchema = yup.object().shape({
    email: yup.string().email("Invalid email address").required("Please enter your email"),
    password: yup.string().required("Please enter your password")
});



export default function SignIn() {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const { setAuthData } = useAuth();
    const navigate = useNavigate();
    const handleSubmit = async (values) => {
        setLoading(true);
        try {
            const result = await AuthService.login({ email: values.email, password: values.password });
            const token = result?.token;
            const userProfile = result?.userProfile;
            localStorageHandler.setItem("token", token);
            localStorageHandler.setItem("userProfile", userProfile);
            setAuthData(userProfile);
            navigate("/")
            showToastMessageSuccess("Login successfully")
        } catch (error) {
            showToastMessageError(error);
        } finally {
            setLoading(false);
        }

    }
    return (
        <Container className={classes.container} component="main" maxWidth="md">
            <SpinnerLoading isLoading={loading} />
            <Box
                sx={{
                    // marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Avatar sx={{ bgcolor: 'secondary.main' }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Formik
                    initialValues={initialValues}
                    onSubmit={handleSubmit}
                    validationSchema={validationSchema}
                    validateOnBlur={false}
                >
                    <Form className={classes.form}>
                        <Field
                            type="email"
                            name="email"
                            placeholder="Enter your email"
                            component={TextInput}
                            autoFocus
                            required
                            fullWidth
                            label="Email Address"
                            margin="normal"
                            isFormikField
                        />
                        <Field
                            name="password"
                            placeholder="Enter your password"
                            component={TextInput}
                            required
                            fullWidth
                            label="Password"
                            secure
                            isFormikField
                        />
                        <Field
                            name="isRemember"
                            component={CustomCheckbox}
                            label="Remember me"
                            isFormikField
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Sign In
                        </Button>
                    </Form>
                </Formik>
                <Box>
                    OR
                </Box>
                <Box>
                    <Link to="/register">Register</Link>
                </Box>
            </Box>
        </Container>
    );
}