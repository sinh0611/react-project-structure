import * as React from 'react';
import { CircularProgress } from '@mui/material';
import useStyles from './style';
import './sassStyle.scss';

const SpinnerLoading = ({ isLoading }) => {
    const classes = useStyles();
    if (isLoading) {
        return (
            <div className={`${classes.spinnerLoadingWp} spinner-wrapper`}>
                <div className={`${classes.innerContentWp} spinner-circle`}>
                    <CircularProgress classes={{ colorPrimary: 'spinner-custom-color' }} />
                </div>
            </div>
        );
    }
    return null;
};

export default SpinnerLoading;
