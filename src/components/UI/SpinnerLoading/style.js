
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(theme => ({
    spinnerLoadingWp: {
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        background: 'rgba(0, 0, 0, 0.2)',
        top: 0,
        left: 0,
        zIndex: 99999
    },
    innerContentWp: {
        height: 60,
        width: 60,
        borderRadius: 12,
        background: '#ffffff',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    }
}));

export default useStyles;