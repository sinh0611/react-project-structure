import React, { useState, useEffect } from "react";
import { Box, TextField } from "@mui/material";
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import IconButton from '@mui/material/IconButton';
import { makeStyles } from "@mui/styles";



const useStyles = makeStyles(theme => ({
    errorMsg: {
        color: "red"
    }
}));

const TextInput = React.forwardRef((props, ref) => {
    const {
        value,
        onChange,
        label,
        secure = false,
        InputProps,
        type = "text",
        isFormikField = false,
        field,
        form
    } = props;

    const classes = useStyles();
    const [isShowPw, setIsShowPw] = useState(false);

    const toggleShowPw = () => setIsShowPw(prev => !prev);

    const inputProps = secure ? {
        endAdornment: <InputAdornment position="end">
            <IconButton
                aria-label="toggle password visibility"
                onClick={toggleShowPw}
                edge="end"
            >
                {!isShowPw ? <VisibilityOff /> : <Visibility />}
            </IconButton>
        </InputAdornment>,
        ...InputProps
    } : InputProps;

    const _type = secure ? isShowPw ? "text" : "password" : type;

    const handleChange = (e) => {
        const value = e.target.value;
        if (isFormikField) {
            form?.setFieldValue(field?.name, value);
        } else {
            onChange(value);
        }
    }
    const handleBlur = (e) => {
        if (isFormikField) {
            form?.setTouched({
                [field?.name]: true
            })
        }

    }
    return (
        <Box>
            <TextField
                ref={ref}
                value={isFormikField ? field?.value : value}
                onChange={handleChange}
                onBlur={handleBlur}
                label={label}
                InputProps={inputProps}
                type={_type}
                {...props}
            />
            {
                (isFormikField && form?.errors?.[field?.name] && form?.touched?.[field?.name]) &&
                <div className={classes.errorMsg}>
                    {form?.errors?.[field?.name]}
                </div>
            }
        </Box>

    )
});

export default TextInput;