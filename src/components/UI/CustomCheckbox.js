import React from "react";
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

const CustomCheckbox = React.forwardRef((props, ref) => {
    const {
        label = "Label",
        value,
        onChange,
        isFormikField = false,
        field,
        form
    } = props;

    const handleChange = (e) => {
        const value = e.target.checked;
        if (isFormikField) {
            form?.setFieldValue(field?.name, value);
        } else {
            onChange(value);
        }
    }
    return (
        <FormControlLabel
            control={
                <Checkbox
                    onChange={handleChange}
                    value={isFormikField ? field?.value : value}
                />}
            label={label}
            ref={ref}
        />
    )
});

export default CustomCheckbox;