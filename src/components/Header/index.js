import React from "react";
import "./styles.scss";
import Box from '@mui/material/Box';

const Header = () => {

    return (
        <div className="header">
            <img src="/assets/imgs/icon-back.svg" />
            <Box
                sx={{
                    width: 300,
                    height: 300,
                    backgroundColor: 'primary.dark',
                    '&:hover': {
                        backgroundColor: 'primary.main',
                        opacity: [0.9, 0.8, 0.7],
                    },
                }}
            />

        </div>
    )
}

export default Header;