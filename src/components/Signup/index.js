import React, { useState } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import TextInput from "@components/UI/TextInput";
import { Field, Form, Formik } from 'formik';
import * as yup from "yup";
import { makeStyles } from "@mui/styles";
import * as AuthService from "@api/auth";
import { Link, useNavigate } from 'react-router-dom';
import { showToastMessageError, showToastMessageSuccess } from '@utils/utilitiesService';
import SpinnerLoading from '@components/UI/SpinnerLoading';



const useStyles = makeStyles(theme => ({
    container: {
        background: "#ffffff",
        padding: "24px 16px",
        borderRadius: 8
    },
    form: {
        minWidth: 440
    }
}));


const initialValues = {
    name: "",
    email: "",
    password: "",
    confirmPassword: ""
}

const validationSchema = yup.object().shape({
    name: yup.string().min(1, "Minimum 1 characters").max(100, "Maximum 100 characters").required(),
    email: yup.string().email("Invalid email address").required("Please enter your email"),
    password: yup.string().required("Please enter your password"),
    confirmPassword: yup.string().oneOf([yup.ref("password"), null], "Password and confirm password must match")
});



export default function Signup() {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const handleSubmit = async (values) => {
        setLoading(true);
        try {
            await AuthService.signup({
                name: values.name,
                email: values.email,
                password: values.password
            });
            showToastMessageSuccess("Register account successully!");
            navigate("/login");
        } catch (error) {
            showToastMessageError(error);
        } finally {
            setLoading(false);
        }
    }
    return (
        <Container className={classes.container} component="main" maxWidth="md">
            <SpinnerLoading isLoading={loading} />
            <Box
                sx={{
                    // marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Avatar sx={{ bgcolor: 'secondary.main' }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <Formik
                    initialValues={initialValues}
                    onSubmit={handleSubmit}
                    validationSchema={validationSchema}
                    validateOnBlur={false}
                >
                    <Form className={classes.form}>
                        <Field
                            type="text"
                            name="name"
                            placeholder="Enter your name"
                            component={TextInput}
                            autoFocus
                            required
                            fullWidth
                            label="Name"
                            margin="normal"
                            isFormikField
                        />
                        <Field
                            type="email"
                            name="email"
                            placeholder="Enter your email"
                            component={TextInput}
                            required
                            fullWidth
                            label="Email Address"
                            margin="normal"
                            isFormikField
                        />
                        <Field
                            name="password"
                            placeholder="Enter your password"
                            component={TextInput}
                            required
                            fullWidth
                            label="Password"
                            secure
                            isFormikField
                        />
                        <Box mt={1}>
                            <Field
                                name="confirmPassword"
                                placeholder="Enter confirm password"
                                component={TextInput}
                                required
                                fullWidth
                                label="Confirm Password"
                                secure
                                isFormikField
                            />
                        </Box>

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Sign up
                        </Button>
                    </Form>
                </Formik>
                <Box>
                    Already Have Account?&nbsp;
                    <Link to="/login">Login</Link>
                </Box>
            </Box>
        </Container>
    );
}