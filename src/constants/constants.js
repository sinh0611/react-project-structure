export const TOAST_TYPES = {
    SUCCESS: 'success',
    ERROR: 'error',
    WARN: 'warn',
};